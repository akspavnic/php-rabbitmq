
### Build containers

```
    docker-compose up -d --build
```

### First terminal run receive

```
    docker-compose exec phpfpm sh -c 'cd scripts && php receive.php' 
```

### Second terminal run send

```
    docker-compose exec phpfpm sh -c 'cd scripts && php send.php' 
```

### Init rabbitmq ui admin (http://localhost:15672)

```
    docker-compose exec rabbitmq sh -c 'rabbitmq-plugins enable rabbitmq_management'
``` 