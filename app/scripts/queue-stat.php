<?php
require_once __DIR__ .'/../bootstrap.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'rabbit', 'rabbit');
$channel = $connection->channel();

$stat = $channel->queue_declare('hello', true);

print_r($stat);

$channel->close();
$connection->close();