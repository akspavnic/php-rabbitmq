<?php
require_once __DIR__ . '/../bootstrap.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use duncan3dc\Forker\Fork;

$callback = function(int $number) {
    $connection = new AMQPStreamConnection('rabbitmq', 5672, 'rabbit', 'rabbit');
    $channel = $connection->channel();

    $stat = $channel->queue_declare('hello', false, true, false, false);
    if ($stat[1] === 0) {
        exit(0);
    }

    echo " [*][{$number}] Waiting for messages. To exit press CTRL+C\n";

    $callback = function ($msg) use ($number, $channel) {
        echo " [consumer][{$number}] Received: ", $msg->body, "\n";
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    };

    try {
        $channel->basic_consume('hello', '', false, false, false, false, $callback);
    } catch (\Exception $e) {
        exit(0);
    }


    while (count($channel->callbacks)) {
        try {
            $channel->wait();
        } catch (\Exception $e) {
            exit(0);
        }
    }

    $channel->close();
    $connection->close();
};

$callback_check_queue = function () {

    $connection = new AMQPStreamConnection('rabbitmq', 5672, 'rabbit', 'rabbit');
    $channel = $connection->channel();

    while (true) {
        $stat = $channel->queue_declare('hello', false, true, false, false);
        if ($stat[1] === 0) {
            $channel->queue_delete('hello');
            exit(0);
        }

        print_r($stat);

        if ($stat[1] > 0) {
            sleep(1);
        } else {
            $channel->queue_delete('hello');
        }
    }

    $channel->close();
    $connection->close();
};

$setup_queue = function () {
    $connection = new AMQPStreamConnection('rabbitmq', 5672, 'rabbit', 'rabbit');
    $channel = $connection->channel();

    $channel->queue_declare('hello', false, true, false, false);

    for ($i = 0; $i < 10000; $i++) {
        $m = "Hello World [{$i}]!";
        $msg = new AMQPMessage($m, [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ]);
        $channel->basic_publish($msg, '', 'hello');
        echo " [{$i}] Sent 'Hello World!'\n";
    }

    $channel->close();
    $connection->close();
};

$fork = new Fork;

// setup queue
$fork->call($setup_queue);
$fork->wait();
sleep(5);

// callback checking that queue not empty.
$fork->call($callback_check_queue);

// workers
for ($n = 0; $n < 3; $n++) {
    $fork->call($callback, $n+1);
}

echo "Waiting for the threads to finish...\n";
$fork->wait();
echo "End\n";