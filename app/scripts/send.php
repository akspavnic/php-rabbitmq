<?php
require_once __DIR__ .'/../bootstrap.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'rabbit', 'rabbit');
$channel = $connection->channel();

$channel->queue_declare('hello', false, true, false, false);

for ($i = 0; $i < 3000; $i++) {
    $m = "Hello World [{$i}]!";
    $msg = new AMQPMessage($m, [
        'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
    ]);
    $channel->basic_publish($msg, '', 'hello');
    echo " [{$i}] Sent 'Hello World!'\n";
}

$channel->close();
$connection->close();